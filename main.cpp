/**
* @brief Fenetre principale
* @details  Ce fichier contien le programme qui va gerer la fenetre
* @author HMZ LRB
* @date  1/02/24
* @version 0.9
* @file dialog.cpp
*
*/

#include "dialog.h"
#include <QApplication>
#include <QLCDNumber>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog win1; //create a windows


    win1.setFixedSize(1200,800);
    win1.show(); //show w windows
   // win1.adjustSize();
   // win1.showFullScreen();




    a.exec(); //its wait a event
    return EXIT_SUCCESS;
}
