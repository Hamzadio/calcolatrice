/**
* @brief Fonction des boutons
* @details  Ce fichier contien le corps des fonctions et les connects utilisées pour generer la "Calculatrice"
* @author HMZ LRB
* @date  1/02/24
* @version 0.9
* @file dialog.cpp
*
*/

#include <QPushButton>
#include <QApplication>
#include <QLCDNumber>

#include "dialog.h"


/**
 * @brief Dialog::Dialog
 * @param parent
 *
 * @details Permet de créer la fenêtre
 */
Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{



    number.setParent(this);
    number.display(numero);
    number.setGeometry(500,150,200,100);
    number.setDecMode();


    //number.setDigitCount(8);
    //number.setBinMode();

    Bin.setParent(this);
    Bin.setText("Binaire");
    Bin.setGeometry(500,250,50,50);
    Bin.setDisabled(false);

    Deci.setParent(this);
    Deci.setText("Decimale");
    Deci.setGeometry(550,250,50,50);
    Deci.setDisabled(true);

    Hex.setParent(this);
    Hex.setText("Hexadecimal");
    Hex.setGeometry(625,250,80,50);
    Hex.setDisabled(false);


    Quit.setGeometry(650,450,50,50);
    Quit.setParent(this); //Quit is the child of "Dialog" the button will be in the "Dialog windows"
    Quit.setText("Quitter"); // set tex on Quit

    plus.setGeometry(750,150,50,50);
    plus.setParent(this);
    plus.setText("+");


    minus.setGeometry(750,200,50,50);
    minus.setParent(this);
    minus.setText("-");

    connect(&plus,
            &QPushButton::clicked,
            this, //in our tool "box"
            &Dialog::incrementer); //Only the name of the fonction and the name of the box(Dialog in this case)

    connect(&minus,
            &QPushButton::clicked,
            this, //in our tool "box"
            &Dialog::decrementer);

    connect(&Quit,
            &QPushButton::clicked,
            qApp,
            &QApplication::quit);


    connect(&Bin,
            &QPushButton::clicked,
            &number,
            &QLCDNumber::setBinMode);

    connect(&Bin,
            &QPushButton::clicked,
            &Bin,
            &QPushButton::setEnabled);

    connect(&Bin,
            &QPushButton::clicked,
            &Deci,
            &QPushButton::setDisabled);

    connect(&Bin,
            &QPushButton::clicked,
            &Hex,
            &QPushButton::setDisabled);



    connect(&Deci,
            &QPushButton::clicked,
            &number,
            &QLCDNumber::setDecMode);

    connect(&Deci,
            &QPushButton::clicked,
            &Deci,
            &QPushButton::setEnabled);

    connect(&Deci,
            &QPushButton::clicked,
            &Bin,
            &QPushButton::setDisabled);

    connect(&Deci,
            &QPushButton::clicked,
            &Hex,
            &QPushButton::setDisabled);



    connect(&Hex,
            &QPushButton::clicked,
            &number,
            &QLCDNumber::setHexMode);

    connect(&Hex,
            &QPushButton::clicked,
            &Hex,
            &QPushButton::setEnabled);

    connect(&Hex,
            &QPushButton::clicked,
            &Bin,
            &QPushButton::setDisabled);

    connect(&Hex,
            &QPushButton::clicked,
            &Deci,
            &QPushButton::setDisabled);
/*
    degriser.setParent(this);
    degriser.setText("Cliquer pour degriser");
    degriser.move(0,50);

    Reveal.setParent(this);
    Reveal.setText("Cliquer pour de-chacher");
    Reveal.setDisabled(true); //
    Reveal.move(150,50);

    Quit.setParent(this); //Quit is the child of "Dialog" the button will be in the "Dialog windows"
    Quit.setText("ABATAFUCING"); // set tex on Quit
    Quit.hide();  //hided
    Quit.move(300,50); //x=300 y 0


    connect(&degriser,
            &QPushButton::clicked, //false
            &Reveal,
            &QPushButton::setDisabled);

    connect(&Reveal,
            &QPushButton::clicked,
            &Quit,
            &QPushButton::show);

    connect(&Quit,
            &QPushButton::clicked,
            qApp,
            &QApplication::quit);




    connect(&Quit,             //Quit is adress the button;
    SIGNAL(clicked()),
    qApp,
    SLOT((quit()));

    this is the ancient sintax of writing an event

*/





}

/**
 *
 * @fn void incrementer(int numero)
 *
 * @brief Dialog::incrementer
 * @details Cette fonction permet de ajouter 1 au QLCD qui affiche un numero
 *
 * @param[in] numero
 */
void Dialog::incrementer(){

    numero = numero +1;
    number.display(numero);

}


/**
*
* @fn void decrementer(int numero)
*
* @brief Dialog::decrementer
* @details Cette fonction permet de soustraire 1 au QLCD qui affiche un numero
*
*
* @param[in] numero
*
*/
void Dialog::decrementer(){

    numero = numero - 1;
    number.display(numero);


}

/**
 * @brief Dialog::~Dialog
 *
 * @details Permet de detruire la fenêtre
 */
Dialog::~Dialog()
{


}

